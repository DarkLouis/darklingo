//
//  ViewController.swift
//  Doulingo2.0
//
//  Created by Luigi Borriello on 06/12/2019.
//  Copyright © 2019 Luigi Borriello. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController
{
    @IBOutlet weak var badWords: UIButton!
    @IBOutlet weak var ProgressBar: UIProgressView!
    @IBOutlet weak var tastoSpeaker: UIButton!
    @IBOutlet weak var C1: UIButton!
    @IBOutlet weak var C2: UIButton!
    @IBOutlet weak var C3: UIButton!
    @IBOutlet weak var C4: UIButton!
    @IBOutlet weak var B1: UIButton!
    @IBOutlet weak var B2: UIButton!
    @IBOutlet weak var B3: UIButton!
    @IBOutlet weak var B5: UIButton!
    @IBOutlet weak var B6: UIButton!
    @IBOutlet weak var B7: UIButton!
    @IBOutlet weak var pc1: UIButton!
    @IBOutlet weak var pc2: UIButton!
    @IBOutlet weak var pc3: UIButton!
    @IBOutlet weak var pc4: UIButton!
    @IBOutlet weak var checkb: UIButton!
    @IBOutlet weak var Lifes: UILabel!
    @IBOutlet weak var sfondo: UIView!
    @IBOutlet weak var titolo: UILabel!
    @IBOutlet weak var testo: UILabel!
    @IBOutlet weak var hicon: UILabel!
    @IBOutlet weak var speakerrusk: UIButton!
    
    var infoButtons = [UIButton]()
    var answerButtons = [UIButton]()
    var pickedChoiceButtons = [UIButton]()
    
    var pressedButtons = 0
    var indexWord = 0
    var checkedButtons = 0
    var lastRand = -1
    var player: AVAudioPlayer?
    var lastBadChoice = 0
    
    var lives: Int = 5
    var points: Float = 0.0
    var helps: Int = 5
    
    struct resultArray
    {
        var text: String = ""
        var color: UIColor = .clear
        var background: UIColor = .clear
    }
    
    func result(show: Bool, title: resultArray, text: resultArray, bg: resultArray)
    {
        titolo.text = title.text
        titolo.textColor = title.color
        titolo.backgroundColor = title.background
        
        testo.text = text.text
        testo.textColor = text.color
        testo.backgroundColor = text.background
        
        sfondo.backgroundColor = bg.background
        
        if(show)
        {
            speakerrusk.isHidden = false
            sfondo.isHidden = false
            titolo.isHidden = false
            testo.isHidden = false
        }
        else
        {
            speakerrusk.isHidden = true
            sfondo.isHidden = true
            titolo.isHidden = true
            testo.isHidden = true
        }
    }
    
    func SpeakText(text: String, locale: String = "en-US")
    {
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: locale)
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
    
    struct words : Codable
    {
        var Russian : String
        var English : String
    }
    
    var Words = [words]()
    
    func downloadWords()
    {
        var url : URL
        
        if(lastBadChoice == 0)
        {
            url = URL(string: "https://darklouis.eu/m/lmlr/json.php?bw=0")!
            badWords.setTitle("No bad words", for: [])
            badWords.backgroundColor = .systemGreen
        }
        else
        {
            url = URL(string: "https://darklouis.eu/m/lmlr/json.php?bw=1")!
            badWords.setTitle("Bad words", for: [])
            badWords.backgroundColor = .systemRed
            lastBadChoice = 2
        }
        
        let data = try! Data(contentsOf: url, options: [])
        Words = try! JSONDecoder().decode(Array<words>.self, from: data)
    }
    
    func find(toFind: String, into: String) -> Bool
    {
        if(into.contains(toFind))
        {
            return true
        }
        return false
    }
    
    @objc func removeBlock()
    {
        for btn in answerButtons
        {
            btn.isHidden = false
        }
    }
    
    @objc func badWFunc()
    {
        if(lastBadChoice == 0)
        {
            badWords.setTitle("Bad words", for: [])
            badWords.backgroundColor = .systemRed
            lastBadChoice = 1
        }
        else
        {
            badWords.setTitle("No bad words", for: [])
            badWords.backgroundColor = .systemGreen
            lastBadChoice = 0
        }
    }
    
    override func viewDidLoad()
    {
        if(Words.isEmpty || lastBadChoice == 1)
        {
            print("CALL")
            downloadWords();
        }
        
        if(lastBadChoice == 2)
        {
            badWords.setTitle("Bad words", for: [])
            badWords.backgroundColor = .systemRed
        }
        
        overrideUserInterfaceStyle = .light
        
        result(
            show: false,
            title: resultArray(),
            text: resultArray(),
            bg: resultArray()
        )
        
        sfondo.isHidden = false
        sfondo.backgroundColor = UIColor(white: 1, alpha: 0)
        
        super.viewDidLoad()
        
        badWords.layer.cornerRadius = 10
        
        if(lives == 0 || points >= 1.0)
        {
            Lifes.text = "5"
            hicon.text = "5"
            ProgressBar.setProgress(0.0, animated: false)
            lives = 5
            points = 0.0
            helps = 5
            lastRand = -1
        }
        else
        {
            Lifes.text = String(lives)
            hicon.text = String(helps)
            ProgressBar.setProgress(points, animated: false)
        }
        
        tastoSpeaker.layer.cornerRadius = 12
        tastoSpeaker.addTarget(self, action: #selector(talkSpeaker), for: UIControl.Event.touchDown)
        
        speakerrusk.layer.cornerRadius = 12
        speakerrusk.addTarget(self, action: #selector(talkSpeaker2), for: UIControl.Event.touchDown)
        
        ProgressBar.transform = CGAffineTransform(scaleX: 1, y: 4)
        
        repeat
        {
            indexWord = Words.indices.randomElement()!
        }
        while lastRand == indexWord
        
        lastRand = indexWord
        let word = Words[indexWord].English.components(separatedBy: ",")
        let wordTranslated = Words[indexWord].Russian.components(separatedBy: ",")
        
        pickedChoiceButtons.append(pc1)
        pickedChoiceButtons.append(pc2)
        pickedChoiceButtons.append(pc3)
        pickedChoiceButtons.append(pc4)
        
        checkButton(state: 0)
        checkb.addTarget(self, action: #selector(processButtonClickEvent3), for: UIControl.Event.touchDown)
        badWords.addTarget(self, action: #selector(badWFunc), for: UIControl.Event.touchDown)
        
        for pc in pickedChoiceButtons
        {
            pc.isHidden = true
            pc.setTitle(nil, for: [])
            pc.addTarget(self, action: #selector(processButtonClickEvent2), for: UIControl.Event.touchDown)
        }
        
        answerButtons.append(B1)
        answerButtons.append(B2)
        answerButtons.append(B3)
        answerButtons.append(B5)
        answerButtons.append(B6)
        answerButtons.append(B7)
        
        infoButtons.append(C1)
        infoButtons.append(C2)
        infoButtons.append(C3)
        infoButtons.append(C4)
        
        for ib in infoButtons
        {
            ib.addTarget(self, action: #selector(tellMe), for: UIControl.Event.touchDown)
            ib.isHidden = true
        }
        
        var i = 0
        
        while(i < word.count)
        {
            infoButtons[i].isHidden = false
            i+=1
        }
        
        i = 0
        
        for chars in word
        {
            infoButtons[i].setTitle(chars, for: [])
            infoButtons[i].underline()
            i += 1
        }
        
        Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(removeBlock), userInfo: nil, repeats: false)
        
        var randwords = [String]()
        
        i = 0
        
        while(i < wordTranslated.count)
        {
            randwords.append(wordTranslated[i])
            i += 1
        }
        
        for button in answerButtons
        {
            button.layer.cornerRadius = 10
            button.addTarget(self, action: #selector(processButtonClickEvent), for: UIControl.Event.touchDown)
            button.isHidden = true
        }
        
        var randomWorldTranslated = Words[Words.indices.randomElement()!].Russian.components(separatedBy: ",")
        
        while(i < answerButtons.count)
        {
            randomWorldTranslated = Words[Words.indices.randomElement()!].Russian.components(separatedBy: ",")
            randwords.append(randomWorldTranslated[randomWorldTranslated.indices.randomElement()!])
            i += 1
        }
        
        randwords.shuffle()
        
        i = 0
        
        for button in answerButtons
        {
            button.setTitle(randwords[i], for: [])
            i += 1
        }
    }
    
    @objc func processButtonClickEvent(srcObj: UIButton)
    {
        for pc in pickedChoiceButtons
        {
            if(pc.isHidden == true)
            {
                SpeakText(text: srcObj.currentTitle!, locale: "ru-RU")
                pc.isHidden = false
                pc.setTitle(srcObj.currentTitle!, for: [])
                pressedButtons += 1
                checkButton(state: 1)
                break
            }
        }
        
        for ab in answerButtons
        {
            if(ab == srcObj)
            {
                ab.setTitle(nil, for: [])
                ab.isEnabled = false
                ab.backgroundColor = .lightGray
                break
            }
        }
        
        if(pressedButtons == pickedChoiceButtons.endIndex)
        {
            for ab in answerButtons
            {
                ab.isEnabled = false
            }
        }
    }
    
    @objc func talkSpeaker()
    {
        let wordCorrect = Words[indexWord].English.replacingOccurrences(of: ",", with: " ", options: NSString.CompareOptions.literal, range: nil)
        SpeakText(text: wordCorrect)
    }
    
    @objc func talkSpeaker2()
    {
        let wordCorrect = Words[indexWord].Russian.replacingOccurrences(of: ",", with: " ", options: NSString.CompareOptions.literal, range: nil)
        SpeakText(text: wordCorrect, locale: "ru-RU")
    }
    
    @objc func processButtonClickEvent2(srcObj: UIButton)
    {
        for pc in pickedChoiceButtons
        {
            if(pc == srcObj)
            {
                pc.setTitle(nil, for: [])
                pc.isHidden = true
                pressedButtons -= 1
            }
        }
        
        for ab in answerButtons
        {
            if(ab.currentTitle == nil)
            {
                ab.isEnabled = true
                ab.setTitle(srcObj.titleLabel!.text, for: [])
                ab.backgroundColor = .none
                break
            }
        }
        
        if(pressedButtons == 0) { checkButton(state: 0) }
        else { checkButton(state: 1) }
    }
    
    @objc func removeRed()
    {
        hicon.textColor = .link
        player?.stop()
    }
    
    @objc func tellMe(srcObj: UIButton)
    {
        let w = Words[indexWord].English.components(separatedBy: ",")
        let wt = Words[indexWord].Russian.components(separatedBy: ",")
        var i = 0

        if(helps == 0)
        {
            i = 0
            
            for word in wt
            {
                if(word == srcObj.titleLabel!.text)
                {
                    srcObj.underline(text: w[i])
                    break
                }
                
                i += 1
            }
            
            if(hicon.textColor != .red)
            {
                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(removeRed), userInfo: nil, repeats: false)
                hicon.textColor = .red
                
                playSound(name: "nohelps", ext: "mp3")
            }
        }
        else
        {
            for word in w
            {
                if(word == srcObj.titleLabel!.text!)
                {
                    srcObj.underline(text: wt[i])
                    helps -= 1
                    hicon.text = String(helps)
                    break
                }
                
                i += 1
            }
            
            i = 0
            
            for word in wt
            {
                if(word == srcObj.titleLabel!.text)
                {
                    srcObj.underline(text: w[i])
                    break
                }
                
                i += 1
            }
        }
    }
    
    func playSound(name: String, ext: String)
    {
        guard let url = Bundle.main.url(forResource: name, withExtension: ext) else { return }

        do
        {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)

            guard let player = player else { return }

            player.play()

        }
        catch let error
        {
            print(error.localizedDescription)
        }
    }
    
    @objc func processButtonClickEvent3(srcObj: UIButton)
    {
        var answer = [String]()
        
        if(titolo.isHidden == true)
        {
            for pc in pickedChoiceButtons
            {
                if(pc.isHidden == false)
                {
                    answer.append(pc.titleLabel!.text!)
                }
            }
        
            let exWord = Words[indexWord].Russian.components(separatedBy: ",")
        
            if(answer.count >= exWord.count)
            {
                for word in exWord
                {
                    if(word == answer[checkedButtons])
                    {
                        checkedButtons += 1
                    }
                    else
                    {
                        break
                    }
                }
            }
            
            if(checkedButtons == answer.count)
            {
                result(
                    show: true,
                    title: resultArray(text: "Congratulations", color: .systemGreen),
                    text: resultArray(text: "You are correct", color: .systemGreen),
                    bg: resultArray(background: UIColor(red: 0, green: 255, blue: 0, alpha: 0.2))
                )
                
                playSound(name: "correct", ext: "wav")
                
                checkButton(state: 3)
                
                tastoSpeaker.isEnabled = false
                
                for ab in answerButtons
                {
                    ab.isEnabled = false
                }
                
                for ib in infoButtons
                {
                    ib.isEnabled = false
                }
                
                for pc in pickedChoiceButtons
                {
                    pc.isEnabled = false
                }
                
                points += 0.1
                
                ProgressBar.setProgress(points, animated: true)
                
                if(points >= 1.0)
                {
                    result(
                        show: true,
                        title: resultArray(text: "You won", color: .green),
                        text: resultArray(text: "Refresh to start a new game", color: .green),
                        bg: resultArray(background: UIColor(red: 0, green: 255, blue: 0, alpha: 0.2))
                    )
                
                    playSound(name: "won", ext: "mp3")
                }
            }
            else
            {
                let wordCorrect = Words[indexWord].Russian.replacingOccurrences(of: ",", with: " ", options: NSString.CompareOptions.literal, range: nil)
                
                result(
                    show: true,
                    title: resultArray(text: "Correct solution:", color: .red),
                    text: resultArray(text: wordCorrect, color: .red),
                    bg: resultArray(background: UIColor(red: 255, green: 0, blue: 0, alpha: 0.5))
                )
                
                playSound(name: "wrong", ext: "wav")

                tastoSpeaker.isEnabled = false
                checkButton(state: 2)
                
                for ab in answerButtons
                {
                    ab.isEnabled = false
                }
                
                for ib in infoButtons
                {
                    ib.isEnabled = false
                }
                
                for pc in pickedChoiceButtons
                {
                    pc.isEnabled = false
                }
                
                lives -= 1
                Lifes.text = String(lives)
                
                if(lives <= 0)
                {
                    result(
                        show: true,
                        title: resultArray(text: "You lost", color: .red),
                        text: resultArray(text: "Refresh to start a new game", color: .red),
                        bg: resultArray(background: UIColor(red: 255, green: 0, blue: 0, alpha: 0.5))
                    )
                
                    playSound(name: "lost", ext: "wav")
                }
            }
        }
        else
        {
            restartButton()
        }
    }
    
    func checkButton(state: Int)
    {
        if(state == 0)
        {
            checkb.isEnabled = false
            checkb.setTitle("CHECK", for: [])
            checkb.backgroundColor = .lightGray
            checkb.tintColor = .darkGray
            
        }
        else if(state == 1)
        {
            checkb.isEnabled = true
            checkb.backgroundColor = .green
            checkb.tintColor = .black
            checkb.setTitle("CHECK", for: [])
        }
        else if(state == 2)
        {
            checkb.isEnabled = true
            checkb.backgroundColor = .red
            checkb.tintColor = UIColor(red: 196, green: 13, blue: 33, alpha: 1.0)
            checkb.setTitle("CONTINUE", for: [])
        }
        else
        {
            checkb.isEnabled = true
            checkb.backgroundColor = .green
            checkb.tintColor = .black
            checkb.setTitle("CONTINUE", for: [])
        }
    }
    
    @objc func restartButton()
    {
        result(
            show: false,
            title: resultArray(),
            text: resultArray(),
            bg: resultArray()
        )
        
        checkButton(state: 0)

        tastoSpeaker.isEnabled = true
        
        for ab in answerButtons
        {
            ab.isHidden = true
            ab.setTitle(nil, for: [])
            ab.isEnabled = true
        }
        
        for ib in infoButtons
        {
            ib.isHidden = false
            ib.isEnabled = true
            ib.setTitle("", for: [])
        }
        
        for pc in pickedChoiceButtons
        {
            pc.isEnabled = true
            pc.setTitle(nil, for: [])
            pc.isHidden = true
        }
        
        pressedButtons = 0
        indexWord = 0
        checkedButtons = 0
        
        pickedChoiceButtons.removeAll()
        infoButtons.removeAll()
        answerButtons.removeAll()
        
        reloadViewFromNib()
    }
}

extension UIViewController
{
    func reloadViewFromNib()
    {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view)
    }
}

extension UIButton
{
    func underline(text: String? = nil)
    {
        var txt = text
        
        if(text == nil)
        {
            txt = self.titleLabel!.text!
        }
        
        let attributedString = NSMutableAttributedString(string: txt!)
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: [])!, range: NSRange(location: 0, length: txt!.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: [])!, range: NSRange(location: 0, length: txt!.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: txt!.count))
        self.setAttributedTitle(attributedString, for: [])
    }
}

@IBDesignable extension UIButton
{

    @IBInspectable var borderWidth: CGFloat
    {
        set
        {
            layer.borderWidth = newValue
        }
        get
        {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat
        {
        set
        {
            layer.cornerRadius = newValue
        }
        get
        {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor?
    {
        set
        {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get
        {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
