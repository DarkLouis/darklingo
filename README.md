# darkLingo - Let me learn Russian!
A simple application based on Duolingo design that lets people learn Russian words.

It gets data from my website (https://darklouis.eu/m/lmlr) through JSON.
- You can choose if you want to learn bad words or not;
- It has a 'tips system' that will help you understand the words; 
- Every word could be heard through a text-speech button.

-------------------------------------

Una semplice app basata sul design di Duolingo ma che permette alle persone di imparare il Russo.

Ottiene i dati dal mio sito (https://darklouis.eu/m/lmlr) attraverso JSON.
- Puoi scegliere se imparare o meno le parolacce;
- Ha un sistema 'a suggerimenti' che ti permetterà di capire più facilmente le parole;
- Ogni parola può essere ascoltata con un pulsante per il text-speech.